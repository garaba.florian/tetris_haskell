{-# LANGUAGE OverloadedStrings #-}
module Main where

import Tetris
import TetrisData
import Graphics.Blank                     -- import the blank canvas

import Control.Concurrent
import Control.Concurrent.MVar
import Control.Monad
import Control.Monad.STM
import Control.Concurrent.STM.TChan

import Data.Monoid
import Data.Maybe

options = Options 3000 ["keydown"] False [] [] False

newGame = do
  ae <- newActiveElement
  return defaultGame{activeElement=ae}

loop :: (Game-> IO Game) -> IO Game -> IO ()
loop f a = let g x = f x >>= g in (a >>= g) >> return ()

eventToCmd :: Event -> Maybe GameCmd
eventToCmd Event{eType=t, eWhich=(Just n)} | isKeyDown t && n == 40 = Just $ Move South
                                           | isKeyDown t && n == 39 = Just $ Move East
                                           | isKeyDown t && n == 37 = Just $ Move West
                                           | isKeyDown t && n == 38 = Just $ Rotation
                                           | isKeyDown t && n == 80 = Just $ State
                                           where isKeyDown = (==) "keydown"
eventToCmd _                                                        = Nothing

cmdHandler :: GameCmd -> Game -> IO Game
cmdHandler (Move dir) g = move (mdir dir) g
  where
    mdir South = DropDown
    mdir d = MovementDir d
cmdHandler Rotation g   = pure g{activeElement=Tetris.rotate g}
cmdHandler State g = pure g{state=togglePause $ state g} where
  togglePause Paused = Running
  togglePause Running = Paused

eventHandler :: GameEvent -> Game -> IO Game
eventHandler Tick g           | res       = move (MovementDir South) g{counter=c}
                              | otherwise = pure g{counter=c}
                                where (c, res) = tick g
eventHandler (GameEvent ev) g = fromJust $ getFirst (First ((\l -> cmdHandler l g) <$> (eventToCmd ev)) <> First (Just $ pure g))


game :: DeviceContext -> IO Game -> IO ()
game context game = do
  mvar <- newEmptyMVar
  id1 <- forkIO $ forever (do -- keypresses
    ev <- wait context
    putMVar mvar (GameEvent ev)
    )
  id2 <- forkIO $ forever (do -- gravity tick timer
    threadDelay 200000
    putMVar mvar Tick
    )
  loop (\g -> do
    ev <- takeMVar mvar
    g2 <- eventHandler ev g
    send context $ do
      render (RenderOptions (1000,800) (0,0) 35) g2
    --putStrLn $ show g2 -- DEBUG
    return g2) game

main :: IO ()
main = blankCanvas options $ \context -> game context newGame
