# Tetris Haskell

![](screenshot.png)

Created as part of my work as a demonstrator in a university project (Functional languages department), partly used as an example to teach the Haskell language for first-year students (my topic was algebraic data types).

Written entirely in Haskell with the use of a package *blank-canvas*, which exposes a JavaScript canvas on a specified port and sends Canvas API command-analogs to it from Haskell.


## Compilation
The **nix** deterministic build tool is required to be installed on the machine.

```
$ nix-shell
$ ghc -threaded Main.hs TetrisData.hs Tetris.hs
```

## Running
```
$ ./Main
```

Then open *localhost:3000* in a browser window
