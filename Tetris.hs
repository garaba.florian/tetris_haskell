{-# LANGUAGE OverloadedStrings #-}
module Tetris where

import TetrisData
import qualified Graphics.Blank as G
import System.Random
import qualified Data.Text
import qualified Data.List
import qualified Data.Vector

data RenderOptions = RenderOptions { dimensions :: (Double, Double), offset :: (Double, Double), scale :: Double }

class Render a where
  render :: RenderOptions -> a -> G.Canvas ()

instance Render Block where
  render opt b = do
    G.lineWidth 4
    G.strokeStyle $ colName White
    G.fillStyle $ color b
    let x = fromIntegral $ fst $ coord b
    let y = fromIntegral $ snd $ coord b
    let offsetX = fst $ offset opt
    let offsetY = snd $ offset opt
    let s = scale opt
    G.fillRect (offsetX + s * x, offsetY + s * y, s, s)
    G.strokeRect (offsetX + s * x + 1, offsetY + s * y + 1, s - 1, s - 1)

instance Render Tetromino where
  render opt t =
    foldl (>>) (pure ()) $ map (render opt) $ blocks t

instance Render Game where
  render opt g = do
    let xx = fst $ dimensions opt
    let yy = snd $ dimensions opt
    let x = fromIntegral $ fst $ arena g
    let y = fromIntegral $ snd $ arena g
    let s = scale opt
    let startX = 30
    let startY = 30

    G.lineWidth 6
    G.strokeStyle $ colName White
    G.fillStyle $ colName Black
    G.fillRect (0,0,xx,yy)
    G.strokeRect (startX, startY, s*x, s*y)
    foldl (>>) (return ()) $ map (render opt{offset=(startX,startY)}) $ staticBlocks g
    render opt{offset=(startX,startY)} $ activeElement g
    G.font "300% serif"
    G.fillStyle $ colName White
    G.fillText ("SCORE", startX + s*x + 50, yy/2) 
    G.fillText (Data.Text.pack $ show $ score g, startX + s*x + 50, yy/2 + 45) 
    if isPaused g then (do
      G.fillStyle $ colName White
      G.fillRect (startX, yy/2-50, s*x, 60)
      G.fillStyle $ colName Black
      G.fillText ("PAUSED", startX + 60, yy/2)) else
        if isStopped g then (do
          G.fillStyle $ colName White
          G.fillRect (startX, yy/2-50, s*x, 60)
          G.fillStyle $ colName Black
          G.fillText ("GAME OVER", startX + 20, yy/2))
	else return ()
    G.sync

isPaused :: Game -> Bool
isPaused Game{state=s} = s == Paused

isStopped :: Game -> Bool
isStopped Game{state=s} = s == Paused || s == GameOver

tick :: Game -> (Counter, Bool)
tick g | gt > 1 && not (isStopped g) = (c{gravityTick=0}, True)
       | isStopped g                  = (c, False)
       | otherwise                  = (c{gravityTick=gt+1}, False)
			     where
			       c = counter g
			       gt = gravityTick c

boundaryCheck :: Game -> Tetromino -> Bool
boundaryCheck g t = and $ map (insideRect (0,0) (arena g) . coord) (blocks t) <>
      ((\b -> \b2 -> coord b /= coord b2) <$> (blocks t) <*> (staticBlocks g))
      where
        insideRect (a,b) (c,d) (x,y) = x >= a && y >= b && x < c && y < d

uniq :: Eq a => [a] -> [a]
uniq xs = helper [] xs
  where
  helper zs [] = reverse zs
  helper [] (y:ys) = helper [y] ys
  helper (z:zs) (y:ys) | z==y      = helper (z:zs) ys
                       | otherwise = helper (y:z:zs) ys

moveToStatic :: Game -> Tetromino -> (Game, Int)
moveToStatic g t = (g{staticBlocks=filterBlocks allBlocks}, 100 * length fullRows)
  where
  allBlocks = (blocks t) <> staticBlocks g
  width = fst $ arena g
  yRange = uniq $ Data.List.sort $ map (snd . coord) $ blocks t
  isFull y = width == length (filter (\b -> y == snd (coord b)) allBlocks)
  fullRows = filter isFull yRange
  filterBlocks bs = map (dropBlock fullRows) $ filter (\b -> not $ elem (snd $ coord b) fullRows) bs
    where 
      dropBlock :: [Int] -> Block -> Block
      dropBlock xs b = b{coord=(+(length $ filter (> (snd $ coord b)) xs)) <$> coord b}

data MovementDir = MovementDir Direction | DropDown

newBlock g = do
    nae <- newActiveElement
    let (staticMoved, scoreDiff) = moveToStatic g $ activeElement g
    return staticMoved{activeElement=nae, score=(score staticMoved)+scoreDiff, state=if boundaryCheck staticMoved nae then Running else GameOver}

move :: MovementDir -> Game -> IO Game
move DropDown g     = if boundaryCheck g moved 
  then move DropDown g{activeElement=moved}
  else newBlock g
  where moved = moveDir South g
move (MovementDir South) g     = if boundaryCheck g moved then pure g{activeElement=moved} else newBlock g where moved = moveDir South g
move (MovementDir d) g         = if boundaryCheck g moved then pure g{activeElement=moved} else pure g     where moved = moveDir d g

moveDir :: Direction -> Game -> Tetromino
moveDir North g = (activeElement g){tCoords=(\(a,b) -> (a,b-1)) $ tCoords (activeElement g)}
moveDir East g  = (activeElement g){tCoords=(\(a,b) -> (a+1,b)) $ tCoords (activeElement g)}
moveDir South g = (activeElement g){tCoords=(\(a,b) -> (a,b+1)) $ tCoords (activeElement g)}
moveDir West g  = (activeElement g){tCoords=(\(a,b) -> (a-1,b)) $ tCoords (activeElement g)}

rotate :: Game -> Tetromino
rotate g = if boundaryCheck g rotated then rotated else t
  where
    t = activeElement g
    rotated = t{dir=rotateDir $ dir t}
    rotateDir North = East
    rotateDir East = South
    rotateDir South = West
    rotateDir West = North

data GameState = Running | Paused | GameOver deriving Eq
data Counter = Counter { gravityTick :: Int }
data GameEvent = Tick | GameEvent G.Event
data GameCmd = Move Direction | Rotation | State

data Game = Game {
    state :: GameState,
    counter :: Counter,
    score :: Int,
    arena :: (Int,Int),
    activeElement :: Tetromino,
    staticBlocks :: [Block]
  }

instance Show Game where
  show g = show $ tCoords $ activeElement g

defaultGame = Game Running (Counter 0) 0 (10,20) (Tetromino I (0,0) North) []

newActiveElement :: IO Tetromino
newActiveElement = do
   n <- getStdRandom (randomR (0,6))
   return (Tetromino (intToType n) (5,0) North)
   where
     intToType :: Int -> TetrominoType
     intToType 0 = I
     intToType 1 = O
     intToType 2 = T
     intToType 3 = J
     intToType 4 = L
     intToType 5 = S
     intToType 6 = Z
