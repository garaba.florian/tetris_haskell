{-# LANGUAGE OverloadedStrings #-}

module TetrisData where

import qualified Data.Text

data Color = Red | LightBlue | DarkBlue | Yellow | Green | Pink | Orange | Brown | Black | White deriving (Show,Eq)

data Block = Block { col :: Color, coord :: (Int,Int)} deriving (Show,Eq)

data TetrominoType = I | O | T | J | L | S | Z deriving Eq

data Direction = North | East | South | West deriving Eq

data Tetromino = Tetromino { tType :: TetrominoType, tCoords :: (Int, Int), dir :: Direction}

colName :: Color -> Data.Text.Text
colName Red = "red"
colName LightBlue = "#00eaff"
colName DarkBlue = "#0349fc"
colName Yellow = "yellow"
colName Green = "green"
colName Pink = "#ff00ff"
colName Orange = "orange"
colName Black = "black"
colName Brown = "brown"
colName White = "white"

color = colName . col

tetrominoTypeColor :: TetrominoType -> Color
tetrominoTypeColor I = LightBlue
tetrominoTypeColor O = Yellow
tetrominoTypeColor T = Pink
tetrominoTypeColor J = DarkBlue
tetrominoTypeColor L = Orange
tetrominoTypeColor S = Green
tetrominoTypeColor Z = Red

blocksCoords :: TetrominoType -> (Int,Int) -> Direction -> [(Int,Int)]
blocksCoords I (x,y) dir       | dir == North = [ (x-2,y), (x-1,y), (x,y), (x+1,y) ]
                               | dir == South = [ (x-2,y-1), (x-1,y-1), (x,y-1), (x+1,y-1) ]
                               | otherwise    = [ (x,y-2), (x,y-1), (x,y), (x,y+1) ]
blocksCoords O (x,y) _                        = [ (x,y), (x+1,y), (x,y+1), (x+1,y+1) ]
blocksCoords T (x,y) dir       | dir == North = [ (x-1,y), (x,y), (x+1,y), (x,y+1) ]
                               | dir == East  = [ (x,y-1), (x,y), (x,y+1), (x-1,y) ]
                               | dir == South = [ (x-1,y), (x,y), (x+1,y), (x,y-1) ]
                               | dir == West  = [ (x,y-1), (x,y), (x,y+1), (x+1,y) ]
blocksCoords J (x,y) dir       | dir == North = [ (x-1,y), (x,y), (x+1,y), (x+1,y+1) ]
                               | dir == East  = [ (x,y-1), (x,y), (x,y+1), (x-1,y+1) ]
                               | dir == South = [ (x-1,y), (x,y), (x+1,y), (x-1,y-1) ]
                               | dir == West  = [ (x,y-1), (x,y), (x,y+1), (x+1,y-1) ]
blocksCoords L (x,y) dir       | dir == North = [ (x-1,y+1), (x,y+1), (x+1,y+1), (x+1,y) ]
                               | dir == East  = [ (x,y-1), (x,y), (x,y+1), (x+1,y+1) ]
                               | dir == South = [ (x-1,y), (x,y), (x+1,y), (x-1,y+1) ]
                               | dir == West  = [ (x,y-1), (x,y), (x,y+1), (x-1,y-1) ]
blocksCoords S (x,y) dir       | dir == North = [ (x-1,y), (x,y), (x,y+1), (x+1,y+1) ]
                               | dir == South = [ (x-1,y), (x,y), (x,y+1), (x+1,y+1) ]
                               | otherwise    = [ (x+1,y-1), (x+1,y), (x,y), (x,y+1) ]
blocksCoords Z (x,y) dir       | dir == North = [ (x-1,y+1), (x,y+1), (x,y), (x+1,y) ]
                               | dir == South = [ (x-1,y+1), (x,y+1), (x,y), (x+1,y) ]
                               | otherwise    = [ (x-1,y-1), (x-1,y), (x,y), (x,y+1) ]

blocks :: Tetromino -> [Block]
blocks (Tetromino t pos dir) = map (Block (tetrominoTypeColor t)) $ blocksCoords t pos dir
