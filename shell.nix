{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  nativeBuildInputs = [ (pkgs.haskellPackages.ghcWithPackages (p: with p; [ blank-canvas ])) ]; 
}
